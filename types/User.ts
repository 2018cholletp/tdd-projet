interface User {
    name?: string;
    routeList: [];
    userId?: string;
}

export type {User}
