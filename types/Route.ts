import {GeoJSON as GeoJSONType} from "geojson";


interface Route {
    routeId: string;
    userId: string;
    geoJson: GeoJSONType;
    name: string;
}

export type {Route}
