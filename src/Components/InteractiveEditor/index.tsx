import MapComponent from '../MapComponents/map';
import {Geocoder} from "../MapComponents/GeocoderComponent/Geocoder";
import {LatLngBounds} from "leaflet";
import {GeoJSON as GeoJSONType} from "geojson";
import {useState} from "react";
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';


function InteractiveEditor() {
    const [geoJsonInitData, setGeoJsonInitData] = useState<GeoJSONType | null>(null);
    const [bounds, setBounds] = useState<LatLngBounds | null>(null);

    return (
        <div>
            <Geocoder setGeoJsonData={setGeoJsonInitData} setBounds={setBounds}/>
            {geoJsonInitData !== null && bounds !== null &&
                <MapComponent geoJsonData={geoJsonInitData!}
                              setGeoJsonData={setGeoJsonInitData}
                              bounds={bounds}
                />
            }
        </div>
    );
}

export default InteractiveEditor;
