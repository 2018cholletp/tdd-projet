import React, {useEffect, useRef, useState} from "react";
import {GeoJSON as GeoJSONType} from "geojson";
import L, {LatLng, LatLngBounds} from "leaflet";
import axios from "axios";
import PolylineUtil from "polyline-encoded";
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';


const mapboxToken = 'pk.eyJ1IjoibjhkeCIsImEiOiJja2pwNzE0cTMwY2EyMnRuNXExeWtpM2M0In0.94CktFP6v5D0_qjyQh-Dvg';

function Geocoder(props: {
    setGeoJsonData: React.Dispatch<React.SetStateAction<GeoJSONType | null>>
    setBounds: React.Dispatch<React.SetStateAction<LatLngBounds | null>>
}) {
    const [start, setStart] = useState<number[] | null>(null);
    const [end, setEnd] = useState<number[] | null>(null);

    const queryURL = (startPoint: number[], endPoint: number[]) => {
        // API doc: https://docs.mapbox.com/api/navigation/directions/#retrieve-directions
        const template = 'https://api.tiles.mapbox.com/v4/directions/{profile}/{waypoints}.json?instructions=html&geometry=polyline&access_token={token}';
        return L.Util.template(template, {
            token: mapboxToken,
            profile: 'mapbox.walking',
            waypoints: [startPoint, endPoint].join(';')
        });
    };

    const queryGeoJson = () => {
        if (!start || !end) {
            return;
        }
        props.setBounds(new LatLngBounds(new LatLng(start![1], start![0]), new LatLng(end![1], end![0])))
        axios.get(queryURL(start, end)).then(response => {
            let coordinates = PolylineUtil.decode(response.data.routes[0].geometry, 6);
            props.setGeoJsonData({
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {},
                        "geometry": {
                            "type": "LineString",
                            "coordinates": coordinates.map((coordinate: number[]) => ([coordinate[1], coordinate[0]]))
                        }
                    }
                ]
            });
        });
    }

    const startGeocoder = useRef(
        new MapboxGeocoder({
            accessToken: mapboxToken,
            placeholder: "Start"
        })
    );

    const endGeocoder = useRef(
        new MapboxGeocoder({
            accessToken: mapboxToken,
            placeholder: "End"
        })
    );

    useEffect(() => {


        startGeocoder.current.addTo("#start-geocoder");
        endGeocoder.current.addTo("#end-geocoder");

        startGeocoder.current.on('result', (e) => {
            setStart(e.result.center);
        });
        startGeocoder.current.on('clear', () => {
            setStart(null);
        });
        endGeocoder.current.on('result', (e) => {
            setEnd(e.result.center);
        });
        endGeocoder.current.on('clear', () => {
            setEnd(null);
        });
    }, []);

    function resetGeocoders() {
        startGeocoder.current.clear()
        endGeocoder.current.clear()
    }

    return (
        <div id="geocoder-container">
            <div id="start-geocoder"/>
            <div id="end-geocoder"/>
            <button onClick={queryGeoJson}>Query</button>
            <button onClick={resetGeocoders}>Reset</button>
        </div>
    )
}

export {Geocoder}
