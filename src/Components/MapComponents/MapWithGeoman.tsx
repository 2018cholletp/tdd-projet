import React, {useEffect} from "react";
import {MapContainerProps, Popup, useMap} from "react-leaflet";
import {GeoJSON as GeoJSONType} from "geojson";
import L, {GeoJSON, Layer, Polyline} from "leaflet";

import "@geoman-io/leaflet-geoman-free";
import "@geoman-io/leaflet-geoman-free/dist/leaflet-geoman.css";

type PMEditGeoJsonEvent = { target: L.GeoJSON };

interface Props extends MapContainerProps {
    geoJsonData: GeoJSONType|string;
    setGeoJsonData: React.Dispatch<React.SetStateAction<GeoJSONType | null>>,
    waypoints: { [id: string] : string; };
    setWaypoints: React.Dispatch<React.SetStateAction<{[p: string]: string}>>
}

const options = [
    { value: 'turn_left', label: 'Tourner à gauche' },
    { value: 'turn_right', label: 'Tourner à droite' },
    { value: 'sidewalk', label: 'Passage piéton' }
];

const MapWithGeoman: React.FC<Props> = (props) => {
    const {
        children,
        geoJsonData,
        setGeoJsonData,
        waypoints,
        setWaypoints,
        ...mapProps
    } = props;

    const onGeoJsonModified = (geoJson: GeoJSONType) => props.setGeoJsonData(geoJson);
    const onWaypointAdded = (index: string) => { setWaypoints(prevWaypoints => {
        const waypoints = { ...prevWaypoints };
        waypoints[index] = "";
        return waypoints
    }); };
    const onWaypointRemoved = (index: string) => { setWaypoints(prevWaypoints => {
        const waypoints = { ...prevWaypoints };
        delete waypoints[index];
        return waypoints;
    }); };
    const onWaypointModified = (index: string, text: string) => { setWaypoints(prevWaypoints => {
        const waypoints = { ...prevWaypoints };
        waypoints[index] = text;
        return waypoints
    }); };
    const onVertexAdded = (index: number) => {
        const newWaypoints: { [id: string] : string; } = {}
        for (let key in waypoints) {
            let keyNumber = parseInt(key);
            newWaypoints[keyNumber >= index ? (keyNumber + 1).toString() : key ] = waypoints[key];
        }
        setWaypoints(newWaypoints);
    };
    const onVertexRemoved = (index: number) => {
        const newWaypoints: { [id: string] : string; } = {}
        for (let key in waypoints) {
            let keyNumber = parseInt(key);
            if (keyNumber === index) {
                continue;
            }
            newWaypoints[keyNumber > index ? (keyNumber - 1).toString() : key ] = waypoints[key];
        }
        setWaypoints(newWaypoints);
    };



    let geoJsonLayer: GeoJSON|null = null;
    const map = useMap();

    useEffect(() => {
        props.bounds && map.fitBounds(props.bounds)
    }, [props.bounds])


    useEffect(() => {
        if (typeof geoJsonData === 'string') {
            return;
        }
        if (!map.pm) {
            return;
        }
        map.eachLayer((layer => {
            if (layer instanceof L.GeoJSON) {
                map.removeLayer(layer)
            }
        }))

        map.pm.addControls({
            position: 'topleft',
            dragMode: false,
            cutPolygon: false,
            removalMode: false,
            rotateMode: false,
            drawCircle: false,
            drawPolygon: false,
            drawRectangle: false,
            drawCircleMarker: false,
            drawMarker: false,
            drawPolyline: false
        });

        geoJsonLayer = L.geoJSON(geoJsonData as GeoJSONType, {});
        geoJsonLayer.addTo(map);

        geoJsonLayer.on("pm:edit", (e: PMEditGeoJsonEvent) => {
            onGeoJsonModified(e.target.toGeoJSON());
        });

        geoJsonLayer.getLayers().forEach((layer: Layer) => {
            if (!(layer instanceof Polyline)) {
                console.log("Layer is not a polyline");
                return;
            }
            (layer as Polyline)
            .on("pm:vertexclick", (e: any) => {
                if (e.indexPath && !(e.indexPath.toString() in waypoints)) {
                    onWaypointAdded(e.indexPath.toString());
                }
            })
            .on("pm:vertexadded", (e: any) => {
                onVertexAdded(e.indexPath[0]);
            })
            .on("pm:vertexremoved", (e: any) => {
                onVertexRemoved(e.indexPath[0]);
            });
        });
    });

    return (
        <div>{Object.keys(waypoints).map(key => {
            // @ts-ignore
            if ((geoJsonData as GeoJSONType).features[0].geometry.coordinates.length > key) {
                return (<Popup
                    key={`popup-${key}`}
                    position={[
                        // @ts-ignore
                        (geoJsonData as GeoJSONType).features[0].geometry.coordinates[key][1],
                        // @ts-ignore
                        (geoJsonData as GeoJSONType).features[0].geometry.coordinates[key][0],
                    ]}
                    autoClose={false}
                    closeButton={true}
                    closeOnClick={false}
                    closeOnEscapeKey={false}
                    onClose={() => {
                        onWaypointRemoved(key);
                    }}
                >
                    <div>
                        <select
                            value={waypoints[key]}
                            onChange={(e) => {
                                onWaypointModified(key, e.target.value)
                            }}>
                            <option value="" disabled/>
                            {options.map(option => (
                                <option key={`popup-select-option-${option.value}`}
                                        value={option.value}>{option.label}</option>
                            ))}
                        </select>
                    </div>
                </Popup>)
            }})}
        </div>
    );
};

export default MapWithGeoman;
