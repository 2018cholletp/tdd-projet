import {LatLngBounds} from "leaflet";
import React, {useEffect, useState} from "react";
import {MapContainer, LayersControl, TileLayer, useMap} from "react-leaflet";
import {GeoJSON as GeoJSONType} from "geojson";
import './map.css';
import MapWithGeoman from "./MapWithGeoman";
import ControlledTextArea from "../ControlledTextArea";

function MapComponent(props: { geoJsonData: GeoJSONType, setGeoJsonData: React.Dispatch<React.SetStateAction<GeoJSONType | null>>, bounds: LatLngBounds }) {
    const [waypoints, setWaypoints] = useState<{ [id: string]: string; }>({});


    const geoJsonAndWaypoints = () => {
        const fusedData = JSON.parse(JSON.stringify(props.geoJsonData))
        fusedData.features[0].properties["coordinates"] = waypoints;
        return fusedData;
    }

    return (
        <div>
            <div id="map-and-field">
                <MapContainer
                    bounds={props.bounds} className="map">
                    <LayersControl position="topright">
                        <LayersControl.BaseLayer checked name="Mapbox">
                            <TileLayer
                                maxNativeZoom={19}
                                maxZoom={25}
                                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                url="https://api.mapbox.com/styles/v1/n8dx/ckz1ho6bi005h15p1rkg86z7h/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibjhkeCIsImEiOiJja2pwNzE0cTMwY2EyMnRuNXExeWtpM2M0In0.94CktFP6v5D0_qjyQh-Dvg"
                            />
                        </LayersControl.BaseLayer>
                        <LayersControl.BaseLayer name="Satellite">
                            <TileLayer
                                maxNativeZoom={19}
                                maxZoom={25}
                                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                url="https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibjhkeCIsImEiOiJja2pwNzE0cTMwY2EyMnRuNXExeWtpM2M0In0.94CktFP6v5D0_qjyQh-Dvg"
                            />
                        </LayersControl.BaseLayer>
                        <LayersControl.BaseLayer name="OpenStreetMap.Mapnik">
                            <TileLayer
                                maxNativeZoom={19}
                                maxZoom={25}
                                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />
                        </LayersControl.BaseLayer>
                    </LayersControl>
                    <MapWithGeoman
                        geoJsonData={props.geoJsonData}
                        setGeoJsonData={props.setGeoJsonData}
                        waypoints={waypoints}
                        setWaypoints={setWaypoints}
                        bounds={props.bounds}
                    />
                </MapContainer>
                <ControlledTextArea
                    id="json-parsed"
                    style={{backgroundColor: typeof props.geoJsonData === 'string' ? "#c53f3c" : "#a1c48e"}}
                    value={typeof props.geoJsonData === 'string' ? props.geoJsonData : JSON.stringify(props.geoJsonData, undefined, 2)}
                    onChange={(e: any) => {
                        let json: any;
                        try {
                            json = JSON.parse(e.target.value);
                        } catch (error) {
                            props.setGeoJsonData(e.target.value);
                            return;
                        }
                        const value = json as GeoJSONType;
                        if (value !== null) {
                            props.setGeoJsonData(value);
                        } else {
                            props.setGeoJsonData(e.target.value);
                        }
                    }}/>
            </div>
            <div>
                <p>{typeof props.geoJsonData === 'string' ? "" : JSON.stringify(geoJsonAndWaypoints(), undefined, 2)}</p>
            </div>
        </div>
    );
}

export default MapComponent;
