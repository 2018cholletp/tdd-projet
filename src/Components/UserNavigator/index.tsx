import {Dispatch, SetStateAction, useEffect, useState} from "react";
import {getUserList} from "../../Firebase/getUsers";
import {User} from "../../../types/User";

interface Props {
    user: User | undefined
    setUser: Dispatch<SetStateAction<User | undefined>>
}

function UserNavigator (props: Props) {

    const [userList, setUserList] = useState<User[]>([])

    useEffect(() => {
        getUserList().then(userList =>
            setUserList(userList)
        )
    },  [])

    return (
        <div>
            <p>User list</p>
            <p>Selected user : {props.user?.name}</p>
            <ul>{
                userList.map((user)=>
                    <li key={user.userId} onClick={() => props.setUser(user)}>{user.name}</li>
                )
            }</ul>
        </div>
    )
}

export {UserNavigator}
