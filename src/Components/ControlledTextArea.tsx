import React, { useEffect, useRef, useState } from 'react';

const ControlledTextArea = (props: any) => {
    const { value, onChange, disabled, ...rest } = props;
    const [cursor, setCursor] = useState(null);
    const ref = useRef(null);

    useEffect(() => {
        const input = ref.current;
        if (input) (input as any).setSelectionRange(cursor, cursor);
    }, [ref, cursor, value]);

    const handleChange = (e: any) => {
        onChange && onChange(e);
        setCursor(e.target.selectionStart);
    };

    return <textarea ref={ref} value={value} onChange={handleChange} {...rest} disabled={disabled} />;
};

export default ControlledTextArea;
