import {collection, Firestore, getDocs, QueryDocumentSnapshot} from "firebase/firestore";
import {User} from "../../types/User";
import {db} from "./index";

// Using dependency injection in order to mock the database
async function getUsers(db: Firestore) : Promise<User[]>{
    const usersCollection = collection(db, "users");
    const usersSnapshot = await getDocs(usersCollection);
    return usersSnapshot.docs.map(doc => userFirestoreConverter(doc))
}

function userFirestoreConverter(doc?: QueryDocumentSnapshot) : User {
        return {
            routeList: [],
            userId: doc?.id,
            name: doc?.data().name,
        }
}

async function getUserList () : Promise<User[]> {
    return getUsers(db)
}

export {getUserList}
