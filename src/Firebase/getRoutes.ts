import {collection, getDocs, QueryDocumentSnapshot} from "firebase/firestore";
import {Route} from "../../types/Route";
import {Firestore} from "firebase/firestore";
import {db} from "./index";


// Using dependency injection in order to mock the database
async function getRoutes(db: Firestore, userId: string): Promise<Array<Route>>{
    return []
}

async function getRouteList(userId: string): Promise<Array<Route>>{
    return getRoutes(db, userId)
}

export {getRouteList, getRoutes}
