// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCHpVPwla4Xtfj_vSBV23VcnIbjf17cfgU",
    authDomain: "sonarvision.firebaseapp.com",
    projectId: "sonarvision",
    storageBucket: "sonarvision.appspot.com",
    messagingSenderId: "787021432538",
    appId: "1:787021432538:web:ad76f68328edeeda0a47e3",
    measurementId: "G-WTDGQH7F1H"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export {app, db}
