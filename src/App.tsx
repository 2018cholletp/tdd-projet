import './App.css';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
import InteractiveEditor from "./Components/InteractiveEditor";
import {useState} from "react";
import {UserNavigator} from "./Components/UserNavigator";
import {User} from "../types/User";


function App() {
    const [userId, setUserId] = useState<User>();
    const [routeId, setRouteId] = useState<string>();

    return (
        <div className="App">
            <UserNavigator user={userId} setUser={setUserId}/>
            {// Route navigator
            }
            <InteractiveEditor />
        </div>
    )
}

export default App;
